# Reign Challenge

This project is a challenge given by REIGN to Rafael Escalona (me).

## Getting Started

First, you'll had to install the required modules.

```sh
npm install
```

Then you can run the app (both server and client) with the following command:

```sh
npm run all
```

## Docker

Docker compose is also available in this project, you can run the following commands to build the required images:

```sh
docker-compose build
docker-compose up
```

You'll need of course docker and docker-compose installed on your system. Once the commands have been run, docker will build the required environment to run the application.

## Migration

Once app is launched, it will verify your database, if there are no collections (or "articles" collection is empty), it will automatically run the first migration.
