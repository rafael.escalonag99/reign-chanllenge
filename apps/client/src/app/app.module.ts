import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { NewsComponent } from './news/news.component';
import { TimeAgoPipe } from './time-ago.pipe';

@NgModule({
  declarations: [AppComponent, NewsComponent, TimeAgoPipe],
  imports: [BrowserModule, HttpClientModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
