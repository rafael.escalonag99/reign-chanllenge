import { Pipe, PipeTransform } from '@angular/core';
import { formatDistance, parseISO } from 'date-fns';

@Pipe({
  name: 'timeAgo'
})
export class TimeAgoPipe implements PipeTransform {
  transform(date: string | Date) {
    let formatDate: Date;

    if (typeof date === 'string') {
      formatDate = parseISO(date);
    } else {
      formatDate = date;
    }

    return formatDistance(formatDate, new Date(), {
      addSuffix: true
    });
  }
}
