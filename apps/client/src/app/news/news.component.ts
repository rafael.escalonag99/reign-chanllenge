import { Component } from '@angular/core';
import { Article } from '@reign-challenge/api-interfaces';
import { ArticleService } from '../article.service';

@Component({
  selector: 'reign-challenge-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss']
})
export class NewsComponent {
  isLoading = false;
  articles: Article[] = [];

  constructor(private articleService: ArticleService) {
    this.handleGetArticles();
  }

  handleDeleteArticle(id: string) {
    this.articleService.deleteArticle(id).subscribe(() => {
      this.handleGetArticles();
    });
  }

  handleGetArticles() {
    this.isLoading = true;

    this.articleService.getArticles().subscribe(articles => {
      this.isLoading = false;
      this.articles = articles;
    });
  }
}
