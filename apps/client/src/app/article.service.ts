import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Article } from '@reign-challenge/api-interfaces';
import { environment } from '../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ArticleService {
  private readonly apiURL = environment.apiURL + '/articles';

  constructor(private httpClient: HttpClient) {}

  getArticles() {
    return this.httpClient.get<Article[]>(this.apiURL);
  }

  deleteArticle(id: string) {
    return this.httpClient.put<void>(`${this.apiURL}/${id}`, null);
  }
}
