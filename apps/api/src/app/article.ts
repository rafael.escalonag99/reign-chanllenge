import { Prop, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type ArticleDocument = Article & Document;

export class Article {
  @Prop({ required: true })
  parent_id: number;

  @Prop()
  story_title: string;

  @Prop()
  title: string;

  @Prop()
  story_url: string;

  @Prop()
  url: string;

  @Prop()
  author: string;

  @Prop()
  created_at: Date;

  @Prop()
  deleted: boolean;
}

export const ArticleSchema = SchemaFactory.createForClass(Article);
