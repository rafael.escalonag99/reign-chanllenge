import { Controller, Get, Param, Put } from '@nestjs/common';
import { Message } from '@reign-challenge/api-interfaces';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get('hello')
  getData(): Message {
    return this.appService.getData();
  }

  @Get('articles')
  getArticles() {
    return this.appService.getArticles();
  }

  @Put('articles/:id')
  deleteArticle(@Param('id') id: string) {
    return this.appService.deleteArticle(id);
  }
}
