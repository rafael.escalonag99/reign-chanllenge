import { HttpService, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Cron, CronExpression } from '@nestjs/schedule';
import { Message } from '@reign-challenge/api-interfaces';
import { Model } from 'mongoose';
import { Article } from './article';

@Injectable()
export class AppService {
  constructor(
    private httpService: HttpService,
    @InjectModel(Article.name) private articleModel: Model<Article>
  ) {}

  getData(): Message {
    return { message: 'Welcome to api!' };
  }

  @Cron(CronExpression.EVERY_HOUR)
  async syncData() {
    try {
      const hackerNewsResponse = await this.httpService
        .get('https://hn.algolia.com/api/v1/search_by_date?query=nodejs')
        .toPromise();

      await this.articleModel.insertMany(hackerNewsResponse.data.hits, {
        ordered: false
      });
    } catch (error) {
      console.error(error.message);
    }
  }

  getArticles(): Promise<Article[]> {
    return this.articleModel
      .find({
        deleted: { $ne: true }
      })
      .sort({ created_at: -1 })
      .exec();
  }

  deleteArticle(id: string) {
    return this.articleModel.findByIdAndDelete(id).exec();
  }
}
