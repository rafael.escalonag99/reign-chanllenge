/**
 * This is not a production server yet!
 * This is only a minimal backend to get started.
 */

import { Logger } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import axios from 'axios';
import { MongoClient } from 'mongodb';
import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

async function initialMigration() {
  try {
    const client = await MongoClient.connect(environment.databaseURI, {
      useUnifiedTopology: true
    });

    const collection = client.db().collection('articles');
    const documentCount = await collection.countDocuments();

    if (documentCount > 0) {
      await client.close();
      return;
    }

    const hackerNewsResponse = await axios.get(environment.migrationURL);
    await collection.insertMany(hackerNewsResponse.data.hits);
    await client.close();
  } catch (error) {
    console.error(error.message);
  }
}

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const globalPrefix = 'api';

  app.setGlobalPrefix(globalPrefix);
  app.enableCors();

  const port = process.env.PORT || 3333;

  await app.listen(port, () => {
    Logger.log('Listening at http://localhost:' + port + '/' + globalPrefix);
  });

  await initialMigration();
}

bootstrap();
