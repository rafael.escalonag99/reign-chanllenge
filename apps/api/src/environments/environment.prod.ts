export const environment = {
  production: true,
  databaseURI: 'mongodb://database/reign',
  migrationURL: 'https://hn.algolia.com/api/v1/search_by_date?query=nodejs'
};
